defmodule Brfebraban.Remessas.CNAB240.Caixa do
  @moduledoc """
    Modulo para extração das informações do CNAB 240 da Caixa

    Tipo de campo , Sigla , Descrição
    Genério       , G     , Genérico
    Específico    , C     , Títulos em Cobrança

    Formato
      ▪ Tipo de Arquivo: Texto, 240 posições por linha
      ▪ Extensões: .rem (tipo Remessa) ou .txt (tipo Texto)
      ▪ Codificação: ANSI

    Entidades Participantes
      Beneficiário/Cedente
      Banco Beneficiário/Banco Cedente
      Sacador Avalista
      Pagador
      Banco Recebedor

    Remessa
      Segmento (Remessa)
      P (Obrigatório)
      Q (Obrigatório)
      R (Opcional)
      S (Opcional)
      Y (Opcional)
      Y-04 (Opcional)
      Y-08 (Opcional)
      Y-50 (Opcional)
      Y-53 (Opcional)
  """
  require Logger

  @behaviour Brfebraban.CNAB240

  alias Brfebraban.CNAB240
  alias Brfebraban.Util

  def extract_from_file(filename) do
    stream = File.stream!(filename)

    header_lote =
      stream
      |> Util.get_line(1)
      |> extract_header_lote_data()

    versao_header =
      stream
      |> Util.get_line(0)
      |> Util.extractor(163, 3)

    versao_header_lote = header_lote.versao_header_lote

    segmentos = extract_segmentos(stream, versao_header, versao_header_lote)
    chunk_by = chunk_segmentos(segmentos)

    format_data(segmentos, chunk_by, header_lote)
  end

  def extract_segmentos(stream, versao_header, versao_header_lote) do
    stream
    |> Stream.map(&parser(&1, versao_header, versao_header_lote))
    |> Stream.reject(&is_atom/1)
  end

  def chunk_segmentos(stream) do
    stream
    |> Enum.group_by(&(&1.segmento))
    |> Map.keys()
    |> Enum.count()
  end

  def format_data(stream, chunk_by, header_lote) do
    stream
    |> Stream.chunk_every(chunk_by)
    |> Stream.map(fn chunk ->
      chunk
      |> Enum.reduce(%CNAB240{}, fn segmento, acc ->
        Map.merge(acc, segmento)
      end)
      |> Map.merge(header_lote)
    end)
    |> Enum.into([])
  end

  @impl true
  def parser(<< "104", _::binary-size(4), "0", _::binary>>, _, _), do: :header
  def parser(<< "104", _::binary-size(4), "1", _::binary>>, _, _), do: :header_lote
  def parser(<< "104", _::binary-size(4), "3", _::binary-size(5), "P", _::binary >> = line, versao_header, versao_header_lote) do
    extract_segmento_p(line, versao_header, versao_header_lote)
  end
  def parser(<< "104", _::binary-size(4), "3", _::binary-size(5), "Q", _::binary >> = line, _, _) do
    extract_segmento_q(line)
  end
  def parser(<< "104", _::binary-size(4), "3", _::binary-size(5), "R", _::binary >> = line, _, _) do
    extract_segmento_r(line)
  end
  def parser(<< "104", _::binary-size(4), "3", _::binary-size(5), _, _::binary >> = _line, _, _) do
    nil
  end
  def parser(<< "104", _::binary-size(4), "5", _::binary>>, _, _), do: :trailer_lote
  def parser(<< "104", _::binary-size(4), "9", _::binary>>, _, _), do: :trailer

  @doc """
    Registro Tipo 1, Header do lote
  """
  def extract_header_lote_data(line) do
    %{
      banco: Util.extractor(line, 0, 3),
      beneficiario_agencia: Util.extractor(line, 53, 6),
      # beneficiario_conta: Util.extractor(line, 59, 6),
      # beneficiario_conta_digito: Util.extractor(line, 58, 1), # TODO validar versao layout 101 header e 060 header de lote para extrair corretamente o codigo de beneficiario
      beneficiario_cpf_cnpj: Util.extractor(line, 18, 15),
      beneficiario_nome: Util.extractor(line, 73, 30, &String.trim_trailing/1),
      data_emissao: Util.extractor(line, 191, 8, &Util.str_to_date/1),
      mensagem_um: Util.extractor(line, 103, 40, &String.trim_trailing/1),
      mensagem_dois: Util.extractor(line, 143, 40, &String.trim_trailing/1),
      tipo: "CNAB240",
      versao_header_lote: Util.extractor(line, 13, 3)
      # numero_remessa: Util.extractor(line, 183, 8)
    }
  end

  @doc """
    Registro Tipo 3, Segmento P (Obrigatorio) - Dados do Título
  """
  def extract_segmento_p(line, versao_header, versao_header_lote) do
    segmento_p =
      %{
        # lote_servico: Util.extractor(line, 3, 4),
        sequencial: Util.extractor(line, 8, 5),
        # movimento_remesssa: Util.extractor(line, 15, 2),
        beneficiario_codigo: Util.extractor(line, 23, 7),
        numero_documento: Util.extractor(line, 62, 11),
        vencimento: Util.extractor(line, 77, 8, &Util.str_to_date/1),
        valor_titulo: Util.extractor(line, 85, 15, &Util.valor/1),
        especie_titulo: Util.extractor(line, 106, 2, &get_especie_titulo/1),
        identificacao_aceite: Util.extractor(line, 108, 1),
        # juros_codigo: Util.extractor(line, 117, 1),
        # juros_data: Util.extractor(line, 118, 8, &Util.str_to_date/1),
        # juros_valor: Util.extractor(line, 126, 15),
        # desconto_codigo: Util.extractor(line, 141, 1),
        # desconto_data: Util.extractor(line, 142, 8, &Util.str_to_date/1),
        # desconto_valor: Util.extractor(line, 150, 15),
        # valor_iof: Util.extractor(line, 165, 15, &Util.valor/1),
        # valor_abatimento: Util.extractor(line, 180, 15, &Util.valor/1),
        # protesto_codigo: Util.extractor(line, 220, 1),
        # protesto_prazo: Util.extractor(line, 221, 2),
        # devolucao_baixa_codigo: Util.extractor(line, 223, 1),
        # devolucao_baixa_prazo: Util.extractor(line, 224, 3),
        # moeda: Util.extractor(line, 227, 2),
        nosso_numero: Util.extractor(line, 40, 18),
        nosso_numero_dac: nil,
        numero_carteira: nil,
        segmento: "P"
      }

    beneficiario_codigo = beneficiario_codigo_by_layout(segmento_p.beneficiario_codigo, versao_header, versao_header_lote)
    segmento_p = Map.put(segmento_p, :beneficiario_codigo, beneficiario_codigo)

    case check_nosso_numero(segmento_p.nosso_numero) do
      true ->
        segmento_p
        |> Map.put(:nosso_numero, nil)
        |> Map.put(:nosso_numero_dac, nil)
        |> Map.put(:linha_digitavel, nil)
        |> Map.put(:codigo_barras, nil)

      false ->
        linha_digitavel = calcular_linha_digitavel(segmento_p)
        codigo_barras = calcular_codigo_barras(segmento_p)

        segmento_p
        |> Map.put(:linha_digitavel, linha_digitavel)
        |> Map.put(:codigo_barras, codigo_barras)
    end
  end

  defp check_nosso_numero("000000000000000000"), do: true
  defp check_nosso_numero(_), do: false

  @doc """
    Registro Tipo 3, Segmento Q (Obrigatorio) - Dados do Pagador e Sacador/Avalista
  """
  def extract_segmento_q(line) do
    tipo_pessoa = Util.extractor(line, 17, 1, &Util.get_cpf_cnpj/1)
    pagador_cpf_cnpj = Util.extractor(line, 19, 14)

    %{
      # lote_servico: Util.extractor(line, 3, 4),
      sequencial: Util.extractor(line, 8, 5),
      # servico_sequencial_q: Util.extractor(line, 8, 5),
      # servico_segmento_q: Util.extractor(line, 13, 1),
      pagador_tipo_pessoa: tipo_pessoa,
      pagador_cpf_cnpj: validate_tipo_inscricao(tipo_pessoa, pagador_cpf_cnpj), # Na documentação pede para extrair 15 campos, estão sendo extraídos 14
      pagador_nome: Util.extractor(line, 33, 40, &String.trim_trailing/1),
      pagador_endereco: Util.extractor(line, 73, 40, &String.trim_trailing/1),
      pagador_bairro: Util.extractor(line, 113, 15, &String.trim_trailing/1),
      pagador_cep: Util.extractor(line, 128, 8),
      pagador_cidade: Util.extractor(line, 136, 15, &String.trim_trailing/1),
      pagador_uf: Util.extractor(line, 151, 2),
      segmento: "Q"
      # TODO implementar restante do layout
    }
  end

  @doc """
    Se o valor do desconto, desconto2, desconto3 forem:
      * 1, 3 ou 4 = informar valor;
      * 2, 5 ou 6 = informar percentual;
  """
  def extract_segmento_r(line) do
    %{
      sequencial: Util.extractor(line, 8, 5),
      # servico_sequencial_r: Util.extractor(line, 8, 5),
      # servico_segmento_r: Util.extractor(line, 13, 1),
      # desconto2_codigo: Util.extractor(line, 17, 1),
      # desconto2_data: Util.extractor(line, 18, 8, &Util.str_to_date/1),
      # desconto2_valor: Util.extractor(line, 26, 15, &validate_tipo/2, [param: [tipo: :desconto2]]),
      # desconto3_codigo: Util.extractor(line, 41, 1),
      # desconto3_data: Util.extractor(line, 42, 8, &Util.str_to_date/1),
      # desconto3_valor: Util.extractor(line, 50, 15, &validate_tipo/2, [param: [tipo: :desconto3]]),
      # multa: Util.extractor(line, 65, 1),
      # multa_data: Util.extractor(line, 66, 8, &Util.str_to_date/1),
      # multa_valor: Util.extractor(line, 74, 15, &validate_tipo/2, [param: [tipo: :multa]]),
      mensagem_tres: Util.extractor(line, 99, 40, &String.trim_trailing/1),
      mensagem_quatro: Util.extractor(line, 139, 40, &String.trim_trailing/1),
      segmento: "R"
    }
  end

  @doc """
    02 DM Duplicata Mercantil
    03 DMI Duplicata Mercantil p/ Indicação
    04 DS Duplicata de Serviço
    05 DSI Duplicata de Serviço p/ Indicação
    06 DR Duplicata Rural
    07 LC Letra de Câmbio
    08 NCC Nota de Crédito Comercial
    09 NCE Nota de Crédito à Exportação
    10 NCI Nota de Crédito Industrial
    11 NCR Nota de Crédito Rural
    12 NP Nota Promissória
    13 NPR Nota Promissória Rural
    14 TM Triplicata Mercantil
    15 TS Triplicata de Serviço
    16 NS Nota de Seguro
    17 RC Recibo
    18 FAT Fatura
    19 ND Nota de Débito
    20 AP Apólice de Seguro
    21 ME Mensalidade Escolar
    22 PC Parcela de Consórcio
    23 NF Nota Fiscal
    24 DD Documento de Dívida
    25 CPR Cédula de Produto Rural
    31 CC Cartão de Crédito
    32 BP Boleto Proposta
    99 OU Outros
  """
  @spec get_especie_titulo(String.t()) :: String.t()
  def get_especie_titulo(value) do
    %{
      "02" => "DM",
      "03" => "DMI",
      "04" => "DS",
      "05" => "DSI",
      "06" => "DR",
      "07" => "LC",
      "08" => "NCC",
      "09" => "NCE",
      "10" => "NCI",
      "11" => "NCR",
      "12" => "NP",
      "13" => "NPR",
      "14" => "TM",
      "15" => "TS",
      "16" => "NS",
      "17" => "RC",
      "18" => "FAT",
      "19" => "ND",
      "20" => "AP",
      "21" => "ME",
      "22" => "PC",
      "23" => "NF",
      "24" => "DD",
      "25" => "CPR",
      "31" => "CC",
      "32" => "BP",
      "99" => "OU"
    } |> Map.get(value)
  end

  def beneficiario_codigo_by_layout(beneficiario_codigo, versao_header, versao_header_lote) do
    case {versao_header, versao_header_lote} do
      {"050", "030"} ->
        beneficiario_codigo

      {"101", "060"} ->
        beneficiario_codigo
        |> binary_part(0, 6)
        |> String.to_integer()
        |> calcular_beneficiario_codigo()
    end
  end

  def calcular_beneficiario_codigo(beneficiario_codigo) when beneficiario_codigo > 999999, do: beneficiario_codigo
  def calcular_beneficiario_codigo(beneficiario_codigo) do
    digito_verificador_beneficiario_codigo = calcular_digito_verificador_beneficiario_codigo(beneficiario_codigo)

    beneficiario_codigo
    |> Integer.to_string()
    |> Kernel.<>(digito_verificador_beneficiario_codigo)
  end

  def calcular_codigo_barras(cobranca) do
    nosso_numero = get_nosso_numero(cobranca.nosso_numero)
    banco = "104"
    moeda = "9"
    # Nota 2 / Anexo I
    dv_geral_codigo_barras = calcular_digito_verificador_geral_codigo_barras(cobranca)
    # Anexo II
    fator_vencimento = calcular_fator_vencimento(cobranca.vencimento)
    valor_documento = valor_to_string_10(cobranca.valor_titulo)

    nosso_numero_s1 = Util.extractor(nosso_numero, 2, 3)
    constante_um = Util.extractor(nosso_numero, 0, 1)
    nosso_numero_s2 = Util.extractor(nosso_numero, 5, 3)
    constante_dois = Util.extractor(nosso_numero, 1, 1)
    nosso_numero_s3 = Util.extractor(nosso_numero, 8, 9)
    dv_campo_livre = calcular_campo_livre_codigo_barras(cobranca)

    banco
    |> Kernel.<>(moeda)
    |> Kernel.<>(dv_geral_codigo_barras)
    |> Kernel.<>(fator_vencimento)
    |> Kernel.<>(valor_documento)
    |> Kernel.<>(cobranca.beneficiario_codigo)
    |> Kernel.<>(nosso_numero_s1)
    |> Kernel.<>(constante_um)
    |> Kernel.<>(nosso_numero_s2)
    |> Kernel.<>(constante_dois)
    |> Kernel.<>(nosso_numero_s3)
    |> Kernel.<>(dv_campo_livre)
  end

  def calcular_linha_digitavel(cobranca) do
    nosso_numero = get_nosso_numero(cobranca.nosso_numero)
    banco = "104"
    moeda = "9"
    beneficiario_codigo_part_um = binary_part(cobranca.beneficiario_codigo, 0, 5)
    beneficiario_codigo_part_dois = binary_part(cobranca.beneficiario_codigo, 5, 2)
    nosso_numero_s1 = Util.extractor(nosso_numero, 2, 3)
    constante_um = Util.extractor(nosso_numero, 0, 1)
    nosso_numero_s2 = Util.extractor(nosso_numero, 5, 3)
    constante_dois = Util.extractor(nosso_numero, 1, 1)
    nosso_numero_s3 = Util.extractor(nosso_numero, 8, 9)
    dv_campo_livre = calcular_campo_livre_codigo_barras(cobranca)
    dv_geral_codigo_barras = calcular_digito_verificador_geral_codigo_barras(cobranca)
    fator_vencimento = calcular_fator_vencimento(cobranca.vencimento)
    valor_documento = valor_to_string_10(cobranca.valor_titulo)

    campo_um =
      banco
      |> Kernel.<>(moeda)
      |> Kernel.<>(beneficiario_codigo_part_um)

    dv_campo_um = calcular_campo_nove_pos(campo_um)
    bloco_um = campo_um <> dv_campo_um

    campo_dois =
      beneficiario_codigo_part_dois
      |> Kernel.<>(nosso_numero_s1)
      |> Kernel.<>(constante_um)
      |> Kernel.<>(nosso_numero_s2)
      |> Kernel.<>(constante_dois)

    dv_campo_dois = calcular_campo_dez_pos(campo_dois)
    bloco_dois = campo_dois <> dv_campo_dois

    campo_tres = nosso_numero_s3 <> dv_campo_livre
    dv_campo_tres = calcular_campo_dez_pos(campo_tres)
    bloco_tres = campo_tres <> dv_campo_tres

    bloco_um
    |> Kernel.<>(bloco_dois)
    |> Kernel.<>(bloco_tres)
    |> Kernel.<>(dv_geral_codigo_barras)
    |> Kernel.<>(fator_vencimento)
    |> Kernel.<>(valor_documento)
  end

  def get_nosso_numero("0" <> nosso_numero), do: nosso_numero
  def get_nosso_numero("9" <> nosso_numero), do: nosso_numero
  def get_nosso_numero(nosso_numero), do: nosso_numero

  def calcular_digito_verificador_beneficiario_codigo(beneficiario_codigo) when is_integer(beneficiario_codigo) do
    beneficiario_codigo
    |> Integer.to_string()
    |> calcular_digito_verificador_beneficiario_codigo()
  end

  def calcular_digito_verificador_beneficiario_codigo(beneficiario_codigo) do
    salt = [
      2,
      3,
      4,
      5,
      6,
      7
    ]

    digito_verificador =
      beneficiario_codigo
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.zip(salt)
      |> Enum.map(&Util.multiply_tuple/1)
      |> Enum.sum()

    case digito_verificador < 11 do
      true ->
        11 - digito_verificador

      false ->
        digito_verificador
        |> Kernel.rem(11)
        |> Kernel.-(11)
        |> Kernel.abs()
        |> case do
          n when n in [10, 11] -> "0"
          n -> to_string(n)
        end
    end
  end

  def calcular_campo_livre_codigo_barras(cobranca) do
    nosso_numero = get_nosso_numero(cobranca.nosso_numero)
    beneficiario_codigo = binary_part(cobranca.beneficiario_codigo, 0, 6) # Nota 3 / Anexo VI
    dv_beneficiario_codigo =
      cobranca.beneficiario_codigo
      |> binary_part(0, 6)
      |> calcular_digito_verificador_beneficiario_codigo()

    nosso_numero_s1 = Util.extractor(nosso_numero, 2, 3)
    constante_um = Util.extractor(nosso_numero, 0, 1)
    nosso_numero_s2 = Util.extractor(nosso_numero, 5, 3)
    constante_dois = Util.extractor(nosso_numero, 1, 1)
    nosso_numero_s3 = Util.extractor(nosso_numero, 8, 9)

    salt = [
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
    ]

    campo_livre =
      beneficiario_codigo
      |> Kernel.<>(dv_beneficiario_codigo)
      |> Kernel.<>(nosso_numero_s1)
      |> Kernel.<>(constante_um)
      |> Kernel.<>(nosso_numero_s2)
      |> Kernel.<>(constante_dois)
      |> Kernel.<>(nosso_numero_s3)

    total_soma =
      campo_livre
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.zip(salt)
      |> Enum.map(&Util.multiply_tuple/1)
      |> Enum.sum()

    case total_soma < 11 do
      true ->
        11 - total_soma

      false ->
        total_soma
        |> Kernel.rem(11)
        |> Kernel.-(11)
        |> Kernel.abs()
        |> case do
          n when n in [10, 11] -> "0"
          n -> to_string(n)
        end
    end
  end

  def calcular_digito_verificador_geral_codigo_barras(cobranca) do
    salt = [
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4
    ]

    nosso_numero = get_nosso_numero(cobranca.nosso_numero)
    fator_vencimento = calcular_fator_vencimento(cobranca.vencimento)
    valor = valor_to_string_10(cobranca.valor_titulo)
    nosso_numero_s1 = Util.extractor(nosso_numero, 2, 3)
    constante_um = Util.extractor(nosso_numero, 0, 1)
    nosso_numero_s2 = Util.extractor(nosso_numero, 5, 3)
    constante_dois = Util.extractor(nosso_numero, 1, 1)
    nosso_numero_s3 = Util.extractor(nosso_numero, 8, 9)
    dv_campo_livre = calcular_campo_livre_codigo_barras(cobranca)

    dados_calculo =
      "1049"
      |> Kernel.<>(fator_vencimento)
      |> Kernel.<>(valor)
      |> Kernel.<>(cobranca.beneficiario_codigo)
      |> Kernel.<>(nosso_numero_s1)
      |> Kernel.<>(constante_um)
      |> Kernel.<>(nosso_numero_s2)
      |> Kernel.<>(constante_dois)
      |> Kernel.<>(nosso_numero_s3)
      |> Kernel.<>(dv_campo_livre)

    total_soma =
      dados_calculo
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.zip(salt)
      |> Enum.map(&Util.multiply_tuple/1)
      |> Enum.sum()

    case total_soma < 11 do
      true ->
        11 - total_soma

      false ->
        total_soma
        |> Kernel.rem(11)
        |> Kernel.-(11)
        |> Kernel.abs()
        |> case do
          n when n in [0, 10, 11] -> "1"
          n -> to_string(n)
        end
    end
  end

  def valor_to_string_10(valor) do
    valor
    |> Decimal.from_float()
    |> Decimal.round(2)
    |> to_string()
    |> String.replace(".", "")
    |> String.pad_leading(10, "0")
  end

  def calcular_fator_vencimento(vencimento) do
    Brfebraban.Remessas.CNAB400.Bradesco.fator_vencimento(vencimento)
  end

  def calcular_campo_nove_pos(campo) do
    salt = [
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2
    ]

    total_soma =
      campo
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.zip(salt)
      |> Enum.map(&Util.multiply_tuple/1)
      |> Enum.map(&to_string/1)
      |> Enum.join()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.sum()

    total_soma
    |> Kernel.rem(10)
    |> Kernel.-(10)
    |> Kernel.abs()
    |> case do
      10 -> "0"
      n -> to_string(n)
    end
  end

  def calcular_campo_dez_pos(campo) do
    salt = [
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1
    ]

    total_soma =
      campo
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.zip(salt)
      |> Enum.map(&Util.multiply_tuple/1)
      |> Enum.map(&to_string/1)
      |> Enum.join()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.sum()

    total_soma
    |> Kernel.rem(10)
    |> Kernel.-(10)
    |> Kernel.abs()
    |> case do
      10 -> "0"
      n -> to_string(n)
    end
  end

  @doc """
    Analisa uma string para saber se é um valor percentual ou decimal
    baseado nas opções fornecidas
    Opções: [tipo: :valor_fixo] e [tipo: :percentual]

    Exemplo:
      iex> Caixa.parse_by_valor_or_percentual("150000", [tipo: :valor_fixo])
      1500.00

      iex> Caixa.parse_by_valor_or_percentual("00000006000", [tipo: :percentual])
      0.06

  """
  def parse_by_valor_or_percentual(str, opts \\ []) do
    tipo = Keyword.get(opts, :tipo)
    value = String.replace_leading(str, "0", "")

    tipo
    |> case do
      nil ->
        Logger.error "required argument `tipo` is missing"
        {:error, :missing_tipo_argument}

      :valor_fixo -> Util.valor(value)
      :percentual -> Util.percentual(value)
    end
  end

  def validate_tipo(line, opts \\ []) do
    tipo = Keyword.get(opts, :tipo)

    case tipo do
      nil -> 0.0
      :desconto -> validate_desconto(line, [codigo_desconto: [141, 1], valor_or_percentual: [150, 15]])
      :desconto2 -> validate_desconto(line, [codigo_desconto: [17, 1], valor_or_percentual: [26, 15]])
      :desconto3 -> validate_desconto(line, [codigo_desconto: [41, 1], valor_or_percentual: [50, 15]])
      :juros -> validate_desconto(line, [codigo_desconto: [117, 1], valor_or_percentual: [126, 15]])
      :multa -> validate_multa(line)
    end
  end

  def validate_multa(line) do
    line
    |> Util.extractor(65, 1)
    |> case do
      "0" -> 0.0
      "1" ->
        line
        |> Util.extractor(74, 15)
        |> Util.extractor(74, 15, &parse_by_valor_or_percentual/2, [param: [tipo: :valor_fixo]])
      "2" ->
        line
        |> Util.extractor(74, 15)
        |> Util.extractor(74, 15, &parse_by_valor_or_percentual/2, [param: [tipo: :percentual]])
    end
  end

  def validate_desconto(line, opts \\ []) do
    [cod_start, cod_length] = Keyword.get(opts, :codigo_desconto)
    [vop_start, vop_length] = Keyword.get(opts, :valor_or_percentual)

    line
    |> Util.extractor(cod_start, cod_length)
    |> case do
      "0" -> 0.0
      x when x in ~w[1 3 4] ->
        line
        |> Util.extractor(vop_start, vop_length)
        |> Util.extractor(vop_start, vop_length, &parse_by_valor_or_percentual/2, [param: [tipo: :valor_fixo]])
      x when x in ~w[2 5 6] ->
        line
        |> Util.extractor(vop_start, vop_length)
        |> Util.extractor(vop_start, vop_length, &parse_by_valor_or_percentual/2, [param: [tipo: :percentual]])
    end
  end

  def remove_nil_data(map) do
    map
    |> Map.from_struct()
    |> Enum.filter(fn {_, v} -> v != nil end)
    |> Enum.into(%{})
  end

  def validate_tipo_inscricao("CPF", pagador_cpf_cnpj), do: String.slice(pagador_cpf_cnpj, 3, 13)
  def validate_tipo_inscricao("CNPJ", pagador_cpf_cnpj), do: pagador_cpf_cnpj
end
