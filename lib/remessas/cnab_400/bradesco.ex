defmodule Brfebraban.Remessas.CNAB400.Bradesco do
  @moduledoc """
    Remessa:
    Registro 0 - Header Label
    Registro 1 - Transação
    Registro 2 - Mensagem (opcional)
    Registro 3 - Rateio de Crédito (opcional)
    Registro 6 - Múltiplas Transferências/Débito Automático Informações Complementares
    Registro 7 - Beneficiário Final
    Registro 9 - Trailler
  """
  alias Brfebraban.CNAB400
  alias Brfebraban.Remessas.CNAB400.Bradesco
  alias Brfebraban.Util

  @behaviour Brfebraban.CNAB400
  @banco "237"
  @moeda "9"

  def extract_from_file(filename) do
    stream = File.stream!(filename)

    header_lote =
      stream
      |> Util.get_line(0)
      |> extract_header_label()

    data = extract_lines(stream)
    chunk_by = chunk_by_lines(data)

    format_data(data, chunk_by, header_lote)
  end

  def extract_lines(stream) do
    stream
    |> Stream.map(&parser/1)
    |> Enum.reject(&is_atom/1)
  end

  # Verificar uma forma de n'ao ler o arquivo todo
  def chunk_by_lines(stream) do
    stream
    |> Enum.group_by(fn data ->
      case data do
        {:ok, data} -> data.segmento
        #implementar algum log dos processos
        {:error, _error, _binary} -> nil
      end
    end)
    |> Map.keys()
    |> Enum.reject(&is_nil/1)
    |> Enum.count()
  end

  def format_data(stream, chunk_by, header_lote) do
    stream
    |> Stream.chunk_every(chunk_by)
    |> Stream.map(fn chunk ->
      chunk
      |> validate_row()
      |> Enum.reduce(%{}, fn {:ok, segmento}, acc ->
        Map.merge(acc, segmento)
      end)
      |> merge_header_lote(header_lote)
    end)
    |> Enum.into([])
    |> Enum.reject(&is_nil/1)
  end

  @doc """
    TODO salvar os erros de processamento em uma tabela
    nao existe tabela, retornar o erro de processamento
  """
  @spec validate_row([{:ok, any} | {:error, any, any}, ...]) :: [{:ok, any}, ...] | %{}
  def validate_row([{:error, _error, _sequencial}, {:error, _error2, _sequencial2}]), do: %{}
  def validate_row([{:error, _error, _sequencial}, {:ok, _transacao2}]), do: %{}
  def validate_row([{:ok, _transacao}, {:error, _error, _sequencial2}]), do: %{}
  def validate_row([{:ok, _transacao}, {:ok, _transacao2}] = row), do: row

  @spec merge_header_lote(map, any) :: nil | map
  def merge_header_lote(map, _header_lote) when map_size(map) == 0, do: nil
  def merge_header_lote(data, header_lote) do
    Map.merge(data, header_lote)
  end

  @impl true
  def parser(<< "01REMESSA01COBRANCA", _::binary>>), do: :header
  def parser(<< "1", _::binary>> = line) do
    try do
      data =
        line
        |> Util.replace_non_ascii_characters()
        |> extract_transacao()

      {:ok, data}
    rescue
      error -> {:error, error, line}
    end
  end

  def parser(<< "2", _::binary>> = line) do
    try do
      data =
        line
        |> Util.replace_non_ascii_characters()
        |> extract_transacao_dois()

      {:ok, data}
    rescue
      error -> {:error, error}
    end
  end
  def parser(<< "9", _::binary>>), do: :trailer

  def extract_transacao(line) do
    pagador_tipo = Util.extractor(line, 218, 2, &Util.get_cpf_cnpj/1)
    pagador_cpf_cnpj = Util.extractor(line, 220, 14)

    cnab400 =
      %CNAB400{
        linha: line,
        beneficiario_agencia: Util.extractor(line, 24, 5, &Bradesco.validate_agencia/1),
        beneficiario_conta: Util.extractor(line, 29, 7),
        beneficiario_conta_digito: Util.extractor(line, 36, 1),
        nosso_numero: Util.extractor(line, 70, 11),
        nosso_numero_dac: Util.extractor(line, 81, 1),
        numero_carteira: Util.extractor(line, 21, 3, &Bradesco.validate_numero_carteira/1),
        numero_controle: Util.extractor(line, 37, 25),
        numero_documento: Util.extractor(line, 110, 10, &String.trim_trailing/1),
        valor_titulo: Util.extractor(line, 126, 13, &Util.valor/1),
        vencimento: Util.extractor(line, 120, 6, &Util.str_to_date/1),
        especie_titulo: Util.extractor(line, 147, 2, &Bradesco.get_especie_titulo/1),
        identificacao_aceite: Util.extractor(line, 149, 1),
        data_emissao: Util.extractor(line, 150, 6, &Util.str_to_date/1),
        pagador_tipo_pessoa: pagador_tipo,
        pagador_cpf_cnpj: Util.validate_tipo_inscricao(pagador_tipo, pagador_cpf_cnpj),
        pagador_nome: Util.extractor(line, 234, 40, &String.trim_trailing/1),
        pagador_endereco: Util.extractor(line, 274, 40, &String.trim_trailing/1),
        # pagador_mensagem_um: Util.extractor(line, 314, 12, &String.trim_trailing/1),
        pagador_cep: Util.extractor(line, 326, 8, &String.trim_trailing/1),
        # pagador_mensagem_dois: Util.extractor(line, 334, 60, &String.trim_trailing/1),
        sequencial: Util.extractor(line, 394, 6),
        segmento: Util.extractor(line, 0, 1),
        tipo: "CNAB400"
      }

    case check_nosso_numero(cnab400.nosso_numero) do
      true ->
        cnab400
        |> Map.put(:nosso_numero, nil)
        |> Map.put(:nosso_numero_dac, nil)
        |> Map.put(:linha_digitavel, nil)
        |> Map.put(:codigo_barras, nil)

      false ->
        linha_digitavel = calcular_linha_digitavel(cnab400)
        codigo_barras = calcular_codigo_barras(cnab400)

        cnab400
        |> Map.put(:linha_digitavel, linha_digitavel)
        |> Map.put(:codigo_barras, codigo_barras)
    end
  end

  def check_nosso_numero("00000000000"), do: true
  def check_nosso_numero(_), do: false

  def extract_transacao_dois(line) do
    %CNAB400{
      mensagem_um: Util.extractor(line, 1, 80, &String.trim_trailing/1),
      mensagem_dois: Util.extractor(line, 81, 80, &String.trim_trailing/1),
      mensagem_tres: Util.extractor(line, 161, 80, &String.trim_trailing/1),
      mensagem_quatro: Util.extractor(line, 241, 80, &String.trim_trailing/1),
      segmento: Util.extractor(line, 0, 1),
    }
    |> remove_nil_data()
  end

  def extract_header_label(line) do
    %CNAB400{
      banco: Util.extractor(line, 76, 3),
      beneficiario_nome: Util.extractor(line, 46, 30, &String.trim_trailing/1)
    }
    |> remove_nil_data()
  end

  def remove_nil_data(map) do
    map
    |> Map.from_struct()
    |> Enum.filter(fn {_, v} -> v != nil end)
    |> Enum.into(%{})
  end



  @doc """
    01-Duplicata
    02-Nota Promissória
    03-Nota de Seguro
    05-Recibo
    10-Letras de Câmbio
    11-Nota de Débito
    12-Duplicata de Serv.
    31-Cartão de Crédito
    32-Boleto de Proposta
    33-Depósito e Aporte
    99-Outros
  """
  @spec get_especie_titulo(String.t()) :: String.t()
  def get_especie_titulo(value) do
    %{
      "01" => "DM",
      "02" => "NP",
      "03" => "NS",
      "05" => "REC",
      "10" => "LC",
      "11" => "ND",
      "12" => "DS",
      "31" => "CC",
      "32" => "BP",
      "33" => "DA",
      "99" => ""
    } |> Map.get(value)
  end

  @spec validate_agencia(String.t()) :: String.t()
  def validate_agencia("0" <> agencia), do: agencia
  def validate_agencia(agencia), do: agencia

  @spec validate_numero_carteira(String.t()) :: String.t()
  def validate_numero_carteira("0" <> numero_carteira), do: numero_carteira
  def validate_numero_carteira(numero_carteira), do: numero_carteira

  @doc """
    HERE BE DRAGONS

    a) Multiplica-se cada algarismo do campo pela sequência de multiplicadores 2, 1, 2, 1, 2, 1..., posicionados
    da direita para a esquerda;
    b) Some individualmente, os algarismos dos resultados dos produtos, obtendo-se o total (N);
    c) Divida o total encontrado (N) por 10, e determine o resto da divisão como MOD 10 (N);
    d) Encontre o DAC através da seguinte expressão:
      DAC = 10 – Mod 10 (N)
      OBS.: Se o resultado da etapa d for 10, considere o DAC = 0.
  """
  def calcular_dac_campo(campo) do
    salt = [
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1
    ]

    calculated_number =
      campo
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.zip(salt)
      |> Enum.map(&Util.multiply_tuple/1)
      |> Enum.map(&to_string/1)
      |> Enum.join()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.sum()

    calculated_number
    |> Kernel.rem(10)
    |> Kernel.-(10)
    |> Kernel.abs()
    |> case do
      10 -> "0"
      n -> to_string(n)
    end
  end

  def calcular_dac_geral(linha_digitavel) do
    salt = [
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4
    ]

    calculate_number =
      linha_digitavel
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.zip(salt)
      |> Enum.map(&Util.multiply_tuple/1)
      |> Enum.sum()

    calculate_number
    |> Kernel.rem(11)
    |> Kernel.-(11)
    |> Kernel.abs()
    |> case do
      x when x in [0, 1, 10, 11] -> "1"
      n -> to_string(n)
    end
  end

  def calcular_linha_digitavel(%CNAB400{} = cobranca) do
    beneficiario_agencia = String.pad_leading(cobranca.beneficiario_agencia, 4, "0")
    beneficiario_conta = String.pad_leading(cobranca.beneficiario_conta, 7, "0")
    carteira_inicio = String.slice(cobranca.numero_carteira, 0..0)
    carteira_fim = String.slice(cobranca.numero_carteira, 1..1)
    nosso_numero_inicio = String.slice(cobranca.nosso_numero, 0, 9)
    nosso_numero_fim = String.slice(cobranca.nosso_numero, 9, 2)
    fator_vencimento = fator_vencimento(cobranca.vencimento)
    str_valor_titulo = valor_titulo_to_string(cobranca.valor_titulo)

    campo1 = campo_um(beneficiario_agencia, carteira_inicio)
    campo1_dac = calcular_dac_campo(campo1)

    campo2 = campo_dois(carteira_fim, nosso_numero_inicio)
    campo2_dac = calcular_dac_campo(campo2)

    campo3 = campo_tres(nosso_numero_fim, beneficiario_conta)
    campo3_dac = calcular_dac_campo(campo3)

    digito_verificador_codigo_barras =
      @banco
      |> Kernel.<>(@moeda)
      |> Kernel.<>(fator_vencimento)
      |> Kernel.<>(str_valor_titulo)
      |> Kernel.<>(beneficiario_agencia)
      |> Kernel.<>(cobranca.numero_carteira)
      |> Kernel.<>(cobranca.nosso_numero)
      |> Kernel.<>(beneficiario_conta)
      |> Kernel.<>("0")
      |> calcular_dac_geral()

    campo5 = fator_vencimento <> str_valor_titulo

    composicao_um =
      campo1
      |> Kernel.<>(campo1_dac)
      |> boleto_formatter()

    composicao_dois =
      campo2
      |> Kernel.<>(campo2_dac)
      |> boleto_formatter()

    composicao_tres =
      campo3
      |> Kernel.<>(campo3_dac)
      |> boleto_formatter()

    composicao_um
    |> Kernel.<>(" ")
    |> Kernel.<>(composicao_dois)
    |> Kernel.<>(" ")
    |> Kernel.<>(composicao_tres)
    |> Kernel.<>(" ")
    |> Kernel.<>(digito_verificador_codigo_barras)
    |> Kernel.<>(" ")
    |> Kernel.<>(campo5)
  end

  def calcular_codigo_barras(%CNAB400{} = cobranca) do
    fator_vencimento = fator_vencimento(cobranca.vencimento)
    str_valor_titulo = valor_titulo_to_string(cobranca.valor_titulo)
    beneficiario_agencia = String.pad_leading(cobranca.beneficiario_agencia, 4, "0")
    beneficiario_conta = String.pad_leading(cobranca.beneficiario_conta, 7, "0")

    digito_verificador_codigo_barras =
      @banco
      |> Kernel.<>(@moeda)
      |> Kernel.<>(fator_vencimento)
      |> Kernel.<>(str_valor_titulo)
      |> Kernel.<>(beneficiario_agencia)
      |> Kernel.<>(cobranca.numero_carteira)
      |> Kernel.<>(cobranca.nosso_numero)
      |> Kernel.<>(beneficiario_conta)
      |> Kernel.<>("0")
      |> calcular_dac_geral()

    @banco
    |> Kernel.<>(@moeda)
    |> Kernel.<>(digito_verificador_codigo_barras)
    |> Kernel.<>(fator_vencimento)
    |> Kernel.<>(str_valor_titulo)
    |> Kernel.<>(beneficiario_agencia)
    |> Kernel.<>(cobranca.numero_carteira)
    |> Kernel.<>(cobranca.nosso_numero)
    |> Kernel.<>(beneficiario_conta)
    |> Kernel.<>("0")
  end

  def campo_um(beneficiario_agencia, carteira_inicio), do: "#{@banco}#{@moeda}#{beneficiario_agencia}#{carteira_inicio}"

  def campo_dois(carteira_fim, nosso_numero_inicio), do: "#{carteira_fim}#{nosso_numero_inicio}"

  def campo_tres(nosso_numero_fim, beneficiario_conta), do: "#{nosso_numero_fim}#{beneficiario_conta}0"

  def fator_vencimento(vencimento) do
    vencimento
    |> Timex.diff(~D[1997-10-07], :days)
    |> to_string()
  end

  def valor_titulo_to_string(value) do
    value
    |> Kernel.*(100)
    |> round()
    |> to_string()
    |> String.replace(".", "")
    |> String.pad_leading(10, "0")
  end

  def boleto_formatter(campo) do
    campo
    |> String.split_at(5)
    |> Tuple.to_list()
    |> Enum.join(".")
  end
end
