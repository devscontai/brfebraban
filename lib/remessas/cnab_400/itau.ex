defmodule Brfebraban.Remessas.CNAB400.Itau do
  defstruct linha: "",
            agencia: "",
            conta: "",
            nosso_numero: "",
            numero_carteira: "",
            cliente_id: "",
            cliente_tipo: "",
            vencimento: "",
            codigo_barras: "",
            valor_titulo: ""

  def novo(linha) do
    try do
      client_type =
        case String.slice(linha, 218, 2) do
          "01" -> "cpf"
          "02" -> "cnpj"
        end

      client_id = String.slice(linha, 220, 14)

      client_id =
        case client_type do
          "cpf" -> String.slice(client_id, 3..13)
          "cnpj" -> client_id
        end

      amount_base =
        String.slice(linha, 126, 13)
        |> String.replace_leading("0", "")

      # last 2 digits -> decimal part
      decimal_part =
        String.slice(amount_base, -2..-1)
        |> Integer.parse()
        |> case do
          {n, _} -> n * 0.01
        end

      # first digits (expect 2 last) -> integer part
      amount =
        String.slice(amount_base, 0..-3)
        |> Integer.parse()
        |> case do
          {n, _} -> n + decimal_part
        end

      {:ok, due_date} = Timex.parse(String.slice(linha, 120, 6), "%d%m%y", :strftime)

      agencia = String.slice(linha, 17, 4)
      conta = String.slice(linha, 23, 5)
      nosso_numero = String.slice(linha, 62, 8)
      numero_carteira = String.slice(linha, 83, 3)

      c = %__MODULE__{
        linha: linha,
        agencia: agencia,
        conta: conta,
        nosso_numero: nosso_numero,
        numero_carteira: numero_carteira,
        cliente_id: client_id,
        cliente_tipo: client_type,
        valor_titulo: amount,
        vencimento: due_date
      }

      # IO.puts "nosso numero " <> c.nosso_numero

      %__MODULE__{
        c
        | codigo_barras: calcular_codigo_barras(c)
      }
    rescue
      _ -> {:error, linha}
    end
  end

  @doc """
    HERE BE DRAGONS

    a) Multiplica-se cada algarismo do campo pela sequência de multiplicadores 2, 1, 2, 1, 2, 1..., posicionados
    da direita para a esquerda;
    b) Some individualmente, os algarismos dos resultados dos produtos, obtendo-se o total (N);
    c) Divida o total encontrado (N) por 10, e determine o resto da divisão como MOD 10 (N);
    d) Encontre o DAC através da seguinte expressão:
      DAC = 10 – Mod 10 (N)
      OBS.: Se o resultado da etapa d for 10, considere o DAC = 0.
  """
  def calcular_dac_campo(campo) do
    salt = [
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1
    ]

    reversed_numbers =
      campo
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)

    sum =
      Enum.zip(reversed_numbers, salt)
      |> Enum.map(&multiply_tuple/1)
      |> Enum.map(&to_string/1)
      |> Enum.reduce(fn x, acc -> acc <> x end)
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.sum()

    # IO.puts "dac soma " <> to_string(sum)

    dac =
      case 10 - Kernel.rem(sum, 10) do
        10 -> "0"
        n -> to_string(n)
      end

    # IO.puts " dac total " <> dac

    dac
  end

  def calcular_dac_geral(codigo_barras) do
    salt = [
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ]

    reversed_numbers =
      codigo_barras
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)

    sum =
      Enum.zip(reversed_numbers, salt)
      |> Enum.map(&multiply_tuple/1)
      |> Enum.sum()

    dac =
      case 11 - Kernel.rem(sum, 11) do
        0 -> "1"
        1 -> "1"
        10 -> "1"
        11 -> "1"
        n -> to_string(n)
      end

    dac
  end

  defp multiply_tuple({a, b}), do: a * b

  def calcular_codigo_barras(%__MODULE__{} = cobranca) do
    nosso_numero_inicio = String.slice(cobranca.nosso_numero, 0..1)

    campo1 = "3419#{cobranca.numero_carteira}#{nosso_numero_inicio}"
    campo1_dac = calcular_dac_campo(campo1)

    nosso_numero_fim = String.slice(cobranca.nosso_numero, 2..10)
    agencia_inicio = String.slice(cobranca.agencia, 0..2)

    dac1 =
      "#{cobranca.agencia}#{cobranca.conta}#{cobranca.numero_carteira}#{cobranca.nosso_numero}"

    dac1 = calcular_dac_campo(dac1)
    campo2 = "#{nosso_numero_fim}#{dac1}#{agencia_inicio}"
    campo2_dac = calcular_dac_campo(campo2)

    agencia_fim = String.slice(cobranca.agencia, 3..3)
    conta = cobranca.conta <> calcular_dac_campo(cobranca.conta)
    campo3 = "#{agencia_fim}#{conta}000"
    campo3_dac = calcular_dac_campo(campo3)

    fator_vencimento =
      Timex.diff(cobranca.vencimento, ~N[1997-10-07 00:00:00], :days) |> to_string

    str_valor_titulo =
      round(cobranca.valor_titulo * 100)
      |> to_string
      |> String.replace(".", "")
      |> String.pad_leading(10, "0")

    campo5 = fator_vencimento <> str_valor_titulo

    dac_nosso_numero =
      calcular_dac_campo(
        cobranca.agencia <>
          cobranca.conta <>
          cobranca.numero_carteira <>
          cobranca.nosso_numero
      )

    nosso_numero_verbose =
      cobranca.numero_carteira <>
        cobranca.nosso_numero <>
        dac_nosso_numero

    dac_agencia_conta = calcular_dac_campo(cobranca.agencia <> cobranca.conta)

    campo4 =
      calcular_dac_geral(
        "3419" <>
          fator_vencimento <>
          str_valor_titulo <>
          nosso_numero_verbose <>
          cobranca.agencia <>
          cobranca.conta <>
          dac_agencia_conta <>
          "000"
      )

    # <> "."
    # <> "."
    # <> "."
    # <> "."
    campo1 <>
      campo1_dac <>
      campo2 <>
      campo2_dac <>
      campo3 <>
      campo3_dac <>
      campo4 <>
      campo5
  end
end
