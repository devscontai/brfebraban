defmodule Brfebraban.Remessas.CNAB400.Caixa do
  @moduledoc false

  defstruct linha: "",
            agencia: "",
            conta: "",
            nosso_numero: "",
            numero_carteira: "",
            cliente_id: "",
            cliente_tipo: "",
            vencimento: "",
            codigo_barras: "",
            valor_titulo: ""

  def novo(linha) do
    try do
      agencia = String.slice(linha, 17..20)
      conta = String.slice(linha, 20..26)
      nosso_numero = String.slice(linha, 56..72) |> String.trim() |> String.pad_leading(15, "0")

      client_type =
        case String.slice(linha, 218..219) do
          "01" -> "cpf"
          "02" -> "cnpj"
        end

      client_id = String.slice(linha, 220..233)

      client_id =
        case client_type do
          "cpf" -> String.slice(client_id, 3..13)
          "cnpj" -> client_id
        end

      {:ok, due_date} = Timex.parse(String.slice(linha, 120, 6), "%d%m%y", :strftime)

      amount_base =
        String.slice(linha, 126, 13)
        |> String.replace_leading("0", "")

      # last 2 digits -> decimal part
      decimal_part =
        String.slice(amount_base, -2..-1)
        |> Integer.parse()
        |> case do
          {n, _} -> n * 0.01
        end

      # first digits (expect 2 last) -> integer part
      amount =
        String.slice(amount_base, 0..-3)
        |> Integer.parse()
        |> case do
          {n, _} -> n + decimal_part
        end

      c = %__MODULE__{
        linha: linha,
        agencia: agencia,
        conta: conta,
        nosso_numero: nosso_numero,
        numero_carteira: "01",
        cliente_id: client_id,
        cliente_tipo: client_type,
        valor_titulo: amount,
        vencimento: due_date
      }

      {:ok,
       %__MODULE__{
         c
         | codigo_barras: calcular_codigo_barras(c)
       }}
    rescue
      _ -> {:error, linha}
    end
  end

  @doc """
    HERE BE DRAGONS

    a) Multiplica-se cada algarismo do campo pela sequência de multiplicadores 2, 1, 2, 1, 2, 1..., posicionados
    da direita para a esquerda;
    b) Some individualmente, os algarismos dos resultados dos produtos, obtendo-se o total (N);
    c) Divida o total encontrado (N) por 10, e determine o resto da divisão como MOD 10 (N);
    d) Encontre o DAC através da seguinte expressão:
      DAC = 10 – Mod 10 (N)
      OBS.: Se o resultado da etapa d for 10, considere o DAC = 0.
  """
  def calcular_dac_campo(campo) do
    salt = [
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1,
      2,
      1
    ]

    reversed_numbers =
      campo
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)

    sum =
      Enum.zip(reversed_numbers, salt)
      |> Enum.map(&multiply_tuple/1)
      |> Enum.map(&to_string/1)
      |> Enum.reduce(fn x, acc -> acc <> x end)
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
      |> Enum.sum()

    # IO.puts "dac soma " <> to_string(sum)

    dac =
      case 10 - Kernel.rem(sum, 10) do
        10 -> "0"
        n -> to_string(n)
      end

    # IO.puts " dac total " <> dac

    dac
  end

  def calcular_dac_base11(codigo_barras) do
    salt = [
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ]

    reversed_numbers =
      codigo_barras
      |> String.reverse()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)

    sum =
      Enum.zip(reversed_numbers, salt)
      |> Enum.map(&multiply_tuple/1)
      |> Enum.sum()

    11 - Kernel.rem(sum, 11)
  end

  def calcular_dac_campo_livre(campo_livre) do
    dac_base = calcular_dac_base11(campo_livre)

    cond do
      dac_base <= 9 -> to_string(dac_base)
      dac_base > 9 -> "0"
    end
  end

  def calcular_dac_boleto(codigo_barras) do
    case calcular_dac_base11(codigo_barras) do
      0 -> "1"
      1 -> "1"
      10 -> "1"
      11 -> "1"
      n -> to_string(n)
    end
  end

  defp multiply_tuple({a, b}), do: a * b

  def calcular_codigo_barras(%__MODULE__{} = cobranca) do
    conta = String.pad_leading(cobranca.conta, 7, "0")
    nosso_numero = cobranca.nosso_numero

    campo_livre =
      conta <>
        String.slice(nosso_numero, 2..4) <>
        String.at(nosso_numero, 0) <>
        String.slice(nosso_numero, 5..7) <>
        String.at(nosso_numero, 1) <>
        String.slice(nosso_numero, 8..16)

    campo_livre_dac = calcular_dac_campo_livre(campo_livre)

    campo1 = "1049" <> String.slice(campo_livre, 0..4)
    campo1_dac = calcular_dac_campo(campo1)

    campo2 = String.slice(campo_livre, 5..14)
    campo2_dac = calcular_dac_campo(campo2)

    campo3 = String.slice(campo_livre, 15..24) <> campo_livre_dac
    campo3_dac = calcular_dac_campo(campo3)

    fator_vencimento =
      Timex.diff(cobranca.vencimento, ~N[1997-10-07 00:00:00], :days) |> to_string

    str_valor_titulo =
      round(cobranca.valor_titulo * 100)
      |> to_string
      |> String.replace(".", "")
      |> String.pad_leading(10, "0")

    campo5 = fator_vencimento <> str_valor_titulo

    campo4 =
      calcular_dac_boleto(
        "1049" <>
          fator_vencimento <>
          str_valor_titulo <>
          campo_livre <>
          campo_livre_dac
      )

    campo1 <>
      campo1_dac <>
      "." <>
      campo2 <>
      campo2_dac <>
      "." <>
      campo3 <>
      campo3_dac <>
      "." <>
      campo4 <>
      "." <>
      campo5
  end
end
