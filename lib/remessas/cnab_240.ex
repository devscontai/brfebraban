defmodule Brfebraban.CNAB240 do
  @moduledoc """
    Behaviour for parses data based on pattern matching Tipo [0, 1, 3, 5, 9] wheres
    0 - Header
    1 - Header do lote
    3 - Registros de detalhe segmentos
    5 - Trailer do lote
    9 - Trailer

    In case Tipo equals 3 then pattern matching Segmento remessa [P, Q, ...] wheres
    P - Obrigatorio
    Q - Obrigatorio
  """
  defstruct banco: nil,
            beneficiario_agencia: nil,
            beneficiario_codigo: nil,
            beneficiario_conta: nil,
            beneficiario_conta_digito: nil,
            beneficiario_cpf_cnpj: nil,
            beneficiario_nome: nil,
            codigo_barras: nil,
            data_emissao: nil,
            especie_titulo: nil,
            identificacao_aceite: nil,
            linha: nil,
            linha_digitavel: nil,
            mensagem_um: nil,
            mensagem_dois: nil,
            mensagem_tres: nil,
            mensagem_quatro: nil,
            nosso_numero: nil,
            nosso_numero_dac: nil,
            numero_carteira: nil,
            numero_controle: nil,
            numero_documento: nil,
            pagador_tipo_pessoa: nil,
            pagador_cpf_cnpj: nil,
            pagador_nome: nil,
            pagador_endereco: nil,
            pagador_bairro: nil,
            pagador_cep: nil,
            pagador_cidade: nil,
            pagador_uf: nil,
            sequencial: nil,
            tipo: nil,
            valor_titulo: nil,
            vencimento: nil,
            segmento: nil

  @type t :: %__MODULE__{
    banco: String.t(),
    beneficiario_agencia: String.t(),
    beneficiario_codigo: String.t(),
    beneficiario_conta: String.t(),
    beneficiario_conta_digito: String.t(),
    beneficiario_cpf_cnpj: String.t(),
    beneficiario_nome: String.t(),
    codigo_barras: String.t(),
    data_emissao: Calendar.date(),
    especie_titulo: String.t(),
    identificacao_aceite: String.t(),
    linha: String.t(),
    linha_digitavel: String.t(),
    mensagem_um: String.t() | nil,
    mensagem_dois: String.t() | nil,
    mensagem_tres: String.t() | nil,
    mensagem_quatro: String.t() | nil,
    nosso_numero: String.t(),
    nosso_numero_dac: String.t(),
    numero_carteira: String.t(),
    numero_controle: String.t(),
    numero_documento: String.t(),
    pagador_tipo_pessoa: String.t(),
    pagador_cpf_cnpj: String.t(),
    pagador_nome: String.t(),
    pagador_endereco: String.t(),
    pagador_bairro: String.t(),
    pagador_cep: String.t(),
    pagador_cidade: String.t(),
    pagador_uf: String.t(),
    sequencial: String.t(),
    tipo: String.t(),
    valor_titulo: float(),
    vencimento: Calendar.date(),
    segmento: String.t()
  }

  @type line() :: binary()

  @callback parser(line()) :: atom() | map()
end
