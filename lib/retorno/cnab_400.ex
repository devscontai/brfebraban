defmodule Brfebraban.Retorno.CNAB400 do
  @moduledoc false

  defstruct numero_controle: nil,
            nosso_numero: nil,
            nosso_numero_dac: nil,
            tipo: "CNAB400",
            valor_titulo: nil,
            vencimento: nil

  @type t :: %__MODULE__{
    numero_controle: String.t(),
    nosso_numero: String.t(),
    nosso_numero_dac: String.t(),
    tipo: String.t(),
    valor_titulo: float(),
    vencimento: Calendar.date()
  }

  @type line() :: binary()

  @callback parser(line()) :: atom() | map()
end
