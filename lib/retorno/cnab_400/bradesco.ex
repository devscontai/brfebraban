defmodule Brfebraban.Retorno.CNAB400.Bradesco do
  @moduledoc false

  alias Brfebraban.Retorno.CNAB400
  alias Brfebraban.Util

  @behaviour Brfebraban.Retorno.CNAB400

  def extract_from_file(filename) do
    stream = File.stream!(filename)

    stream
    |> Stream.map(&parser/1)
    |> Enum.reject(&is_atom/1)
  end

  @impl true
  def parser(<< "02RETORNO01COBRANCA", _::binary>>), do: :header
  def parser(<< "1", _::binary>> = line) do
    try do
      retorno = extract_retorno(line)

      {:ok, retorno}
    rescue
      error -> {:error, error}
    end
  end
  def parser(<< "9", _::binary>>), do: :trailer

  def extract_retorno(line) do
    %CNAB400{
      numero_controle: Util.extractor(line, 37, 25),
      nosso_numero: Util.extractor(line, 70, 11),
      nosso_numero_dac: Util.extractor(line, 81, 1),
      valor_titulo: Util.extractor(line, 152, 13, &Util.valor/1),
      vencimento: Util.extractor(line, 146, 6, &Util.str_to_date/1)
    }
  end
end
