defmodule Brfebraban.Retorno.CNAB240 do
  @moduledoc false

  defstruct nosso_numero: nil,
            nosso_numero_dac: nil,
            numero_documento: nil,
            pagador_cpf_cnpj: nil,
            tipo: "CNAB240",
            valor_titulo: nil,
            vencimento: nil


  @type t :: %__MODULE__{
    nosso_numero: String.t(),
    nosso_numero_dac: String.t(),
    numero_documento: String.t(),
    pagador_cpf_cnpj: String.t(),
    tipo: String.t(),
    valor_titulo: float(),
    vencimento: Calendar.date()
  }

  @type line() :: binary()

  @callback parser(line()) :: atom() | map()
end
