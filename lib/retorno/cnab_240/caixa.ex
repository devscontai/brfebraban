defmodule Brfebraban.Retorno.CNAB240.Caixa do
  @moduledoc false

  alias Brfebraban.Retorno.CNAB240
  alias Brfebraban.Util

  @behaviour Brfebraban.Retorno.CNAB240

  def extract_from_file(filename) do
    stream = File.stream!(filename)

    stream
    |> Stream.map(&parser/1)
    |> Stream.reject(&is_atom/1)
    |> Enum.reject(&match?({:error, _}, &1)) # Ignore errors
  end

  @impl true
  def parser(<< "10400000", _::binary>>), do: :header
  def parser(<< "104", _::binary-size(4), "1", "T", _::binary>>), do: :header_lote
  def parser(<< "104", _::binary-size(4), "3", _::binary-size(5), "T", _::binary>> = line) do
    try do
      retorno = extract_retorno(line)

      {:ok, retorno}
    rescue
      error -> {:error, error}
    end
  end
  def parser(<< "104", _::binary-size(4), "3", _::binary-size(5), "U", _::binary>> = _line), do: :segmento_u_ignorado
  def parser(<< "104", _::binary-size(4), "5", _::binary>>), do: :trailer_lote
  def parser(<< "10499999", _::binary>>), do: :trailer

  @spec extract_retorno(binary) :: Brfebraban.Retorno.CNAB240.t()
  def extract_retorno(line) do
    pagador_tipo = Util.extractor(line, 132, 1, &Util.get_cpf_cnpj/1)
    pagador_cpf_cnpj = Util.extractor(line, 134, 14)

    %CNAB240{
      # nosso_numero: Util.extractor(line, 38, 18, &String.trim_leading/1),
      nosso_numero: Util.extractor(line, 38, 18, &get_nosso_numero/1),
      nosso_numero_dac: Util.extractor(line, 56, 1),
      numero_documento: Util.extractor(line, 58, 11),
      pagador_cpf_cnpj: Util.validate_tipo_inscricao(pagador_tipo, pagador_cpf_cnpj),
      valor_titulo: Util.extractor(line, 81, 13, &Util.valor/1),
      vencimento: Util.extractor(line, 73, 8, &Util.str_to_date/1)
    }
  end

  def get_nosso_numero(nosso_numero), do: binary_part(nosso_numero, 1, 17)
end
