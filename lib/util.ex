defmodule Brfebraban.Util do
  @moduledoc false

  @spec get_line(any, any) :: any
  def get_line(stream, line_number) do
    stream
    |> Stream.with_index()
    |> Stream.filter(fn {_value, index} -> index == line_number end)
    |> Enum.at(0)
    |> line()
  end

  @spec extract_numero_compensacao_remessa_400(binary) :: binary
  def extract_numero_compensacao_remessa_400(line) do
    extractor(line, 76, 3)
  end

  @spec extract_numero_compensacao_remessa_240(binary) :: binary
  def extract_numero_compensacao_remessa_240(line) do
    extractor(line, 0, 3)
  end

  def extract_numero_compensacao_retorno_400(line) do
    extractor(line, 76, 3)
  end

  def get_bank_by_compensation_number(compensation_number) do
    %{
      "104" => :caixa,
      "237" => :bradesco,
      "341" => :itau
    }
    |> Map.get(compensation_number)
  end

  @doc """
    Converts string date to date

    Example:
      iex> str_to_date("01012020")
      ~D[2020-01-01]

      iex> str_to_date("010120")
      ~D[2020-01-01]

      iex> str_to_date("00000000")
      nil
  """
  def str_to_date(str) do
    size = byte_size(str)

    str
    |> validate_str_to_date(size)
    |> to_date
  end

  @doc false
  defp validate_str_to_date(str, 6), do: Timex.parse(str, "%d%m%y", :strftime)
  defp validate_str_to_date(str, 8), do: Timex.parse(str, "{D}{0M}{YYYY}")
  defp validate_str_to_date(_, _), do: {:error, :invalid_date}

  @doc false
  defp to_date({:error, _error}), do: nil
  defp to_date({:ok, date}), do: Timex.to_date(date)

  @doc """
    Extrai uma String baseado na posição e comprimento.

    Exemplo:
      iex> extractor("000111222333", 0, 3)
      "000"
  """
  @spec extractor(binary, integer, integer) :: binary
  def extractor(line, start, length) do
    binary_part(line, start, length)
  end

  @doc """
    Extrai uma String baseado na posição e comprimento e aplica uma function caso
    a function seja válida.

    Exemplo:
      iex> extractor("000111222333", 0, 3, &String.to_integer/1)
      0
      iex> extractor("000111222333", 0, 3, "1")
      "000"
  """
  @spec extractor(binary, integer, integer, any) :: any
  def extractor(line, start, length, fun) do
    case is_function(fun) do
      false ->
        extractor(line, start, length)

      true ->
        line
        |> extractor(start, length)
        |> fun.()
    end
  end

  @doc """
    Extrai uma String baseado na posição e comprimento e aplica uma function caso
    a function seja válida, está função pode receber parâmetros.
    É necessário que seja utilizado um Keyword [param: [param: ...]].

    Exemplo:
      iex> extractor("150000", 0, 6, &parse_by_valor_or_percentual/2, [param: [tipo: :valor_fixo]])
      1500.00

      iex> extractor("150000", 0, 6, [], [])
      "150000"
  """
  def extractor(line, start, length, fun, opts \\ []) do
    case is_function(fun) do
      false ->
        extractor(line, start, length)

      true ->
        opt = Keyword.get(opts, :param)
        fun.(line, opt)
    end
  end

  @doc """
    Retorna uma String CPF ou CNPJ caso os valores sejam 1 ou 2 respectivamente.

    Example:
      iex> get_cpf_cnpj("1")
      CPF
      iex> get_cpf_cnpj("2")
      CNPJ
      iex> get_cpf_cnpj("3")
      nil
  """
  def get_cpf_cnpj("1"), do: "CPF"
  def get_cpf_cnpj("2"), do: "CNPJ"
  def get_cpf_cnpj("01"), do: "CPF"
  def get_cpf_cnpj("02"), do: "CNPJ"
  def get_cpf_cnpj(_), do: nil

  @doc """
    Handles Stream.with_index result
  """
  def line({value, _line}), do: value
  def line(_), do: :error

  @doc """
    Trata uma String para que seja transformada em um valor decimal

    Exemplo:
      iex> Caixa.valor("500")
      5.0
  """
  @spec valor(binary) :: float | nil
  def valor(value) when is_binary(value) do
    value
    |> byte_size()
    |> case do
      x when x <= 2 -> String.pad_leading(value, 3, "0")
      x when x > 2 -> value
    end
    |> to_float(-2)
  end
  def valor(_), do: nil

  @doc """
    Trata uma String para que seja transformada em um percentual

    Exemplo:
      iex> Caixa.valor("500")
      0.005
  """
  @spec percentual(binary) :: float
  def percentual(value) when is_binary(value) do
    value
    |> byte_size()
    |> case do
      x when x <= 3 -> String.pad_leading(value, 4, "0")
      x when x > 3 -> value
    end
    |> to_float(-3)
    |> Kernel./(100)
  end
  def percentual(_), do: nil

  @doc """
    Transforma uma String em Float

    Exemplo:
      iex> Caixa.to_float("50000", -2)
      500.0
  """
  @spec to_float(binary, integer) :: float
  def to_float(value, split_at) when is_binary(value) and is_integer(split_at) do
    value
    |> String.split_at(split_at)
    |> Tuple.to_list()
    |> Enum.join(".")
    |> String.to_float()
  end
  def to_float(_, _), do: nil


  @spec remove_non_ascii_characters(binary) :: binary
  def remove_non_ascii_characters(binary) do
    binary
    |> String.to_charlist()
    |> Enum.filter(&(&1 in 0..127))
    |> List.to_string
  end

  def replace_non_ascii_characters(binary) do
    binary
    |> :unicode.characters_to_binary(:latin1)
    |> String.codepoints
    |> Enum.map(fn word ->
      word
      |> String.to_charlist()
      |> hd
      |> Kernel.in(0..127)
      |> case do
        true -> word
        false -> " "
      end
    end)
    |> List.to_string()
  end

  def multiply_tuple({a, b}), do: a * b

  def validate_tipo_inscricao("CPF", pagador_cpf_cnpj), do: String.slice(pagador_cpf_cnpj, 3, 13)
  def validate_tipo_inscricao("CNPJ", pagador_cpf_cnpj), do: pagador_cpf_cnpj
end
