defmodule Brfebraban do
  @moduledoc """
    Documentation for Brfebraban.
    TODO:
      - send a file with 2G size in staging
  """

  alias Brfebraban.Remessas.CNAB240.Caixa, as: CaixaRemessa240
  alias Brfebraban.Remessas.CNAB400.Caixa, as: CaixaRemessa400
  alias Brfebraban.Retorno.CNAB240.Caixa, as: CaixaRetorno240
  alias Brfebraban.Remessas.CNAB400.Bradesco, as: BradescoRemessa400
  alias Brfebraban.Retorno.CNAB400.Bradesco, as: BradescoRetorno400
  alias Brfebraban.Remessas.CNAB400.Itau, as: Itau400

  alias Brfebraban.Util

  @doc """
    Processa um arquivo de remessa sem que seja necessário informar o banco
  """
  @spec processar(binary) :: {:ok, map} | {:info, } | {:error, :invalid_file}
  def processar(filename) do
    stream = File.stream!(filename)
    banco =
      stream
      |> check_cnab()
      |> extract_remessa_banco(stream)

    processar(banco, filename)
  end

  @doc """
    Processa um arquivo de remessa baseado no banco informado e path do arquivo
  """
  @spec processar(:bradesco | :caixa | :itau, binary) :: {:ok, map} | {:info, } | {:error, :invalid_file}
  def processar(:bradesco, filename) do
    case check_cnab(filename) do
      :cnab_400 -> {:ok, BradescoRemessa400.extract_from_file(filename)}
      :cnab_240 -> {:info, :not_implemented}
      :invalid_file -> {:error, :invalid_file}
    end
  end

  def processar(:itau, filename) do
    case check_cnab(filename) do
      :cnab_400 ->
        filename
        |> extrair_corpo()
        |> Enum.map(&Itau400.novo/1)

      :cnab_240 -> {:info, :not_implemented}
      :invalid_file -> {:error, :invalid_file}
    end
  end

  def processar(:caixa, filename) do
    case check_cnab(filename) do
      :cnab_400 ->
        filename
        |> extrair_corpo()
        |> Enum.map(&CaixaRemessa400.novo/1)

      :cnab_240 -> {:ok, CaixaRemessa240.extract_from_file(filename)}
      :invalid_file -> {:error, :invalid_file}
    end
  end

  def processar_retorno(filename) do
    # tipo = check_file_type(filename)
    stream = File.stream!(filename)
    banco =
      stream
      |> check_cnab()
      |> extract_remessa_banco(stream)

    processar_retorno(banco, filename)
  end

  def processar_retorno(:bradesco, filename) do
    case check_cnab(filename) do
      :cnab_400 -> {:ok, BradescoRetorno400.extract_from_file(filename)}
      :cnab_240 -> {:info, :not_implemented}
      :invalid_file -> {:error, :invalid_file}
    end
  end

  def processar_retorno(:caixa, filename) do
    case check_cnab(filename) do
      :cnab_400 -> {:info, :not_implemented}
      :cnab_240 -> {:ok, CaixaRetorno240.extract_from_file(filename)}
      :invalid_file -> {:error, :invalid_file}
    end
  end

  @doc """
    Extrai o número de compensação bancária do header um arquivo de remessa
    baseado em seu tipo de remessa
  """
  @spec extract_remessa_banco(:cnab_240 | :cnab_400 | :invalid_file, any) :: binary | :invalid_file
  def extract_remessa_banco(:cnab_400, stream) do
    stream
    |> Util.get_line(0)
    |> Util.extract_numero_compensacao_remessa_400()
    |> Util.get_bank_by_compensation_number()
  end

  def extract_remessa_banco(:cnab_240, stream) do
    stream
    |> Util.get_line(0)
    |> Util.extract_numero_compensacao_remessa_240()
    |> Util.get_bank_by_compensation_number()
  end
  def extract_remessa_banco(:invalid_file, _), do: :invalid_file

  def extract_retorno_banco(:cnab_400, stream) do
    stream
    |> Util.get_line(0)
    |> Util.extract_numero_compensacao_retorno_400()
    |> Util.get_bank_by_compensation_number()
  end

  @doc """
    Verifica um arquivo de remessa para saber se é um CNAB240 ou CNAB400
  """
  @spec check_cnab(binary | map) :: :cnab_240 | :cnab_400 | :invalid_file
  def check_cnab(filename) when is_binary(filename), do: filename |> File.stream!() |> check_cnab()
  def check_cnab(stream) when is_map(stream) do
    stream
    |> Util.get_line(0)
    |> String.length()
    |> case do
      x when x in [400, 401] -> :cnab_400
      x when x in [240, 241] -> :cnab_240
      _ -> :invalid_file
    end
  end

  # def check_file_type(filename) do
  #   filename
  #   |> String.slice(-3, 3)
  #   |> String.downcase()
  #   |> case do
  #     "rem" -> :rem
  #     "ret" -> :ret
  #   end
  # end

  defp extrair_corpo(fname) do
    lines =
      fname
      |> File.stream!()
      |> Enum.map(&String.trim/1)

    [_header | body] = lines
    [_trailer | body] = Enum.reverse(body)

    filtrar_registros_validos(body)
  end

  defp filtrar_registros_validos(linhas) do
    linhas
    |> Enum.filter(&(String.at(&1, 0) == "1"))
  end
end
