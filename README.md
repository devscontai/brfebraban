# Brfebraban

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `brfebraban` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:brfebraban, "~> 0.1.0"}
  ]
end
```

## Usage

```elixir
  Brfebraban.processar(:bradesco, "/tmp/remessa.rem")
  Brfebraban.processar(:caixa, "/tmp/remessa.rem")
  Brfebraban.processar(:itau, "/tmp/remessa.rem")
```

## Tests

Watcher for TDD:
```
sudo apt-get install inotify-tools
mix test.watch
```

Regular tests:
```
mix test
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/brfebraban](https://hexdocs.pm/brfebraban).
