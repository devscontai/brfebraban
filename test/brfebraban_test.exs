defmodule BrfebrabanTest do
  use ExUnit.Case
  doctest Brfebraban

  describe "bradesco" do
    test "processar with bradesco as bank param" do
      file_path = "#{__DIR__}/fixtures/remessas/bradesco/remessa1.rem"

      assert {:ok, [result]} = Brfebraban.processar(:bradesco, file_path)
      assert result.agencia == "8888"
      assert result.pagador_tipo == "CPF"
    end

    test "processar without bank param" do
      file_path = "#{__DIR__}/fixtures/remessas/bradesco/remessa1.rem"

      assert {:ok, [result]} = Brfebraban.processar(file_path)
      assert result.agencia == "8888"
      assert result.pagador_tipo == "CPF"
    end
  end

  describe "caixa" do
    test "processar with caixa as bank param" do
      file_path = "#{__DIR__}/fixtures/remessas/caixa/remessa1.rem"

      assert [{:ok, result}] = Brfebraban.processar(:caixa, file_path)
      assert result.agencia == "0007"
      assert result.cliente_tipo == "cpf"
    end

    test "processar without bank param" do
      file_path = "#{__DIR__}/fixtures/remessas/caixa/remessa1.rem"

      assert [{:ok, result}] = Brfebraban.processar(file_path)
      assert result.agencia == "0007"
      assert result.cliente_tipo == "cpf"
    end
  end

  @tag :skip
  test "itau" do
    file_path = "#{__DIR__}/fixtures/remessas/caixa/remessa1.rem"

    assert [{:ok, result}] = Brfebraban.processar(:caixa, file_path)
    assert result.agencia == "0007"
    assert result.cliente_tipo == "cpf"
  end
end
