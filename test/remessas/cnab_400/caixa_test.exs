defmodule Brfebraban.Remessas.CNAB400.CaixaTest do
  use ExUnit.Case

  alias Brfebraban.Remessas.CNAB400.Caixa
  test "valid line" do
    str =
      "102238593950001880007777777200043005331                 14666666666666667                                 010143005331  18112000000000200061040000001N071120020000000000000000000000000000000000000000000000000000000000000100009518508658Jordan ramos Santos                     Rua Clemente da Silva, 12               Bouganvile l35703399Sete Lagoas    MG0000000000000000                      00301000002"

    output = {
      :ok,
      %Caixa{
        agencia: "0007",
        cliente_id: "09518508658",
        cliente_tipo: "cpf",
        codigo_barras: "1049777772.77666166648.66666666780.7.84430000020006",
        conta: "7777777",
        linha:
          "102238593950001880007777777200043005331                 14666666666666667                                 010143005331  18112000000000200061040000001N071120020000000000000000000000000000000000000000000000000000000000000100009518508658Jordan ramos Santos                     Rua Clemente da Silva, 12               Bouganvile l35703399Sete Lagoas    MG0000000000000000                      00301000002",
        nosso_numero: "14666666666666667",
        numero_carteira: "01",
        valor_titulo: 200.06,
        vencimento: ~N[2020-11-18 00:00:00]
      }
    }

    assert Caixa.novo(str) == output
  end

  test "invalid line" do
    str = "0000"
    output = {:error, str}

    assert Caixa.novo(str) == output
  end
end
