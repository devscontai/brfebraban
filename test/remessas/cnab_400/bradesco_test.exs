defmodule Brfebraban.Remessas.CNAB400.BradescoTest do
  use ExUnit.Case

  alias Brfebraban.Remessas.CNAB400.Bradesco

  def fixture(:output_final) do
    [
      %Brfebraban.CNAB400{
        agencia: "8888",
        banco: "237",
        beneficiario_nome: "JULIO CESAR MARTINS FREITAS",
        codigo_barras: "23798.88890 95555.555556 56777.777709 2 84320005432110",
        conta: "7777777",
        conta_digito: "6",
        data_emissao: ~D[2020-11-07],
        especie_titulo: "DM",
        identificacao: "N",
        line_number: "2",
        linha: "1                   00990888877777776                         0000000055555555556100000000002               0143004762  07112000000054321100000000001N071120000000000000000000000000000000000000000000000000000000000000000100009518508658Jordan ramos Santos                     RUA CLEMENTE DA SILVA, 12 - BOUGANVILE L            35703399SETE LAGOAS                                                 000002\n",
        mensagem_dois: "",
        mensagem_quatro: "mensagem linha 4",
        mensagem_tres: "mensagem linha 3",
        mensagem_um: "",
        nosso_numero: "55555555556",
        nosso_numero_dac: "1",
        numero_carteira: "99",
        numero_documento: "43004762",
        pagador_cep: "35703399",
        pagador_cpf_cnpj: "09518508658",
        pagador_endereco: "RUA CLEMENTE DA SILVA, 12 - BOUGANVILE L",
        pagador_mensagem_dois: "SETE LAGOAS",
        pagador_mensagem_um: "",
        pagador_nome: "Jordan ramos Santos",
        pagador_tipo: "CPF",
        sequencial: "000002",
        valor_titulo: 54321.1,
        vencimento: ~D[2020-11-07]
      }
    ]
  end

  def fixture(:output_parcial) do
    [
      %Brfebraban.CNAB400{
        agencia: "8888",
        banco: nil,
        beneficiario_nome: nil,
        codigo_barras: "23798.88890 95555.555556 56777.777709 2 84320005432110",
        conta: "7777777",
        conta_digito: "6",
        data_emissao: ~D[2020-11-07],
        especie_titulo: "DM",
        identificacao: "N",
        line_number: "1",
        linha: "1                   00990888877777776                         0000000055555555556100000000002               0143004762  07112000000054321100000000001N071120000000000000000000000000000000000000000000000000000000000000000100009518508658Jordan ramos Santos                     RUA CLEMENTE DA SILVA, 12 - BOUGANVILE L            35703399SETE LAGOAS                                                 000002\n",
        mensagem_dois: nil,
        mensagem_quatro: nil,
        mensagem_tres: nil,
        mensagem_um: nil,
        nosso_numero: "55555555556",
        nosso_numero_dac: "1",
        numero_carteira: "99",
        numero_documento: "43004762",
        pagador_cep: "35703399",
        pagador_cpf_cnpj: "09518508658",
        pagador_endereco: "RUA CLEMENTE DA SILVA, 12 - BOUGANVILE L",
        pagador_mensagem_dois: "SETE LAGOAS",
        pagador_mensagem_um: "",
        pagador_nome: "Jordan ramos Santos",
        pagador_tipo: "CPF",
        sequencial: "000002",
        valor_titulo: 54321.1,
        vencimento: ~D[2020-11-07]
      },
      %{
        line_number: "2",
        mensagem_dois: "",
        mensagem_quatro: "mensagem linha 4",
        mensagem_tres: "mensagem linha 3",
        mensagem_um: ""
      }
    ]
  end

  def fixture(:stream) do
    File.stream!("test/fixtures/remessas/bradesco/remessa1.rem")
  end

  describe "extract_from_file/1" do
    setup [:create_output_final]

    test "valid line", %{output_final: output_final} do
      file = "test/fixtures/remessas/bradesco/remessa1.rem"

      assert Bradesco.extract_from_file(file) == output_final
    end
  end

  describe "extract_lines/1" do
    setup [:create_stream, :create_output_parcial]

    test "extracts lines from given file", %{stream: stream, output_parcial: output_parcial} do
      assert Bradesco.extract_lines(stream) == output_parcial
    end
  end

  @tag :skip
  test "invalid line" do
    str = "0000"
    output = {:error, str}

    assert Bradesco.execute(str) == output
  end

  describe "get_especie_titulo/1" do
    test "Returns a especie_titulo based on param" do
      assert "DM" == Bradesco.get_especie_titulo("01")
    end

    test "Returns nil when especie_titulo is not found on map" do
      assert nil == Bradesco.get_especie_titulo("15")
    end
  end

  describe "validate_agencia/1" do
    test "Returns agencia without 0 at string beginning" do
      assert "8888" == Bradesco.validate_numero_carteira("08888")
    end

    test "Returns agencia when string beginning is different of 0" do
      assert "18888" == Bradesco.validate_numero_carteira("18888")
    end
  end

  describe "validate_numero_carteira/1" do
    test "Returns numero_carteira without 0 at string beginning" do
      assert "99" == Bradesco.validate_numero_carteira("099")
    end

    test "Returns numero_carteira when string beginning is different of 0" do
      assert "199" == Bradesco.validate_numero_carteira("199")
    end
  end

  defp create_output_final(_) do
    output_final = fixture(:output_final)
    %{output_final: output_final}
  end

  defp create_output_parcial(_) do
    output_parcial = fixture(:output_parcial)
    %{output_parcial: output_parcial}
  end

  defp create_stream(_) do
    stream = fixture(:stream)
    %{stream: stream}
  end
end
