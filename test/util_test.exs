defmodule Brfebraban.UtilTest do
  use ExUnit.Case

  alias Brfebraban.Util

  @tag :skip
  describe "get_header/1" do
    test "get header from file" do
      file_path = "#{__DIR__}/fixtures/remessas/bradesco/remessa1.rem"

      assert Util.get_header(file_path) =~ "01REMESSA01COBRANCA"
    end
  end

  describe "get_line/2" do
    test "filter correct line to be extracted" do
      stream = File.stream!("#{__DIR__}/fixtures/remessas/bradesco/remessa1.rem")

      assert Util.get_line(stream, 0) =~ "01REMESSA01COBRANCA"
      assert Util.get_line(stream, 1) =~ "1                   00990888877777776"
    end
  end

  describe "get_compensation_number_400/1" do
    test "get a compensation number from cnab 400 header" do
      stream = File.stream!("#{__DIR__}/fixtures/remessas/bradesco/remessa1.rem")
      header = Util.get_line(stream, 0)

      assert Util.get_compensation_number_400(header) == "237"
    end
  end

  @tag :skip
  describe "get_compensation_number_240/1" do
    test "get a compensation number from cnab 400 header" do
      stream = File.stream!("#{__DIR__}/")
      header = Util.get_line(stream, 0)

      assert Util.get_compensation_number_240(header) == ""
    end
  end

  describe "get_bank_by_compensation_number/1" do
    test "get a bank from compensation number" do
      stream = File.stream!("#{__DIR__}/fixtures/remessas/bradesco/remessa1.rem")
      header = Util.get_line(stream, 0)
      compensation_number = Util.get_compensation_number_400(header)

      assert Util.get_bank_by_compensation_number(compensation_number) == :bradesco
    end
  end

  @tag :skip
  describe "str_to_float/1" do
    test "needs to be implemented" do

    end
  end

  describe "extractor" do
    test "extractor/3 returns sliced string" do
      assert Util.extractor("10102020", 4, 4) == "2020"
    end

    test "extractor/4 returns sliced string then apply function in case that is valid" do
      assert Util.extractor("10102020", 4, 4, &String.to_integer/1) == 2020
    end

    test "extractor/4 returns sliced string when function is not valid" do
      assert Util.extractor("10102020", 4, 4, "") == "2020"
    end
  end

  describe "get_cpf_cnpj/1" do
    test "Returns CPF when params is string 1" do
      assert Util.get_cpf_cnpj("1") == "CPF"
    end

    test "Returns CNPJ when params is string 2" do
      assert Util.get_cpf_cnpj("2") == "CNPJ"
    end

    test "Returns nil when params is not 1 or 2" do
      assert Util.get_cpf_cnpj(1) == nil
      assert Util.get_cpf_cnpj("3") == nil
    end
  end

  describe "str_to_date/1" do
    test "get a converted date" do
      assert ~D[2020-01-10] == Util.str_to_date("100120")
    end

    test "Converts string date to Date when valids" do
      assert Util.str_to_date("01012020") == ~D[2020-01-01]
    end

    test "Returns nil when string date is invalid" do
      assert Util.str_to_date("00002020") == nil
    end
  end

  describe "line/1" do
    test "returns value instead a line number" do
      data = {"01REMESSA01COBRANCA       00000000000000006666JULIO CESAR MARTINS FREITAS   237BRADESCO       071120        MX4444445000001\n", 0}

      assert Util.line(data) =~ "01REMESSA01COBRANCA"
    end
  end

  describe "valor/1" do
    test "converts a string to decimal value" do
      assert 5.0 == Util.valor("500")
    end

    test "returns nil when try to converts non binary" do
      assert nil == Util.valor(500)
    end
  end

  describe "percentual/1" do
    test "converts a string to percentage value" do
      assert 0.005 == Util.percentual("500")
    end

    test "returns nil when try to converts non binary" do
      assert nil == Util.percentual(500)
    end
  end

  describe "to_float/1" do
    test "converts a string to percentage value" do
      assert 50.0 == Util.to_float("500", 2)
    end

    test "returns nil when try to converts non binary" do
      assert nil == Util.to_float(500, 2)
    end
  end
end
