################################################
### Base - used for development
FROM elixir:1.9.4-alpine as base

# install build dependencies
RUN apk add --update git build-base nodejs yarn python inotify-tools alpine-sdk bash openssl

WORKDIR /app
ENV HOME=/app
